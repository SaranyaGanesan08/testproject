FROM openjdk:8
COPY target/*.jar /
ENTRYPOINT ["java", "-jar", "docker-spring-boot.jar"]
